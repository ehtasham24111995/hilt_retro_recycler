package com.example.hiltrecyclerview.ui

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class Base : Application() {
}