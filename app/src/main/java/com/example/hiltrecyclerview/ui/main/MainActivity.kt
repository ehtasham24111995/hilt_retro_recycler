package com.example.hiltrecyclerview.ui.main

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.GridLayoutManager.SpanSizeLookup
import androidx.recyclerview.widget.RecyclerView
import com.example.hiltrecyclerview.data.model.Pokemon
import com.example.hiltrecyclerview.databinding.ActivityMainBinding
import com.example.hiltrecyclerview.di.PokemonInterface
import com.example.hiltrecyclerview.helper.ResponseResource
import com.example.hiltrecyclerview.helper.showToast
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() , PokemonInterface {

    private lateinit var binding: ActivityMainBinding
    private val mainViewModel by viewModels<MainViewModel>()
    @Inject lateinit var pokemonAdapter: PokemonAdapter
    private lateinit var gridLayoutManager: GridLayoutManager
    private var pokemonData: ArrayList<Pokemon.Result?> = arrayListOf()
    //Recycler Load More Variables
    private var loading: Boolean = true
    private var pastVisibleItems: Int = 0
    private var visibleItemCount: Int = 0
    private var totalItemCount: Int = 0
    private var totalCount : Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupRecycler()

        getPokemonData( pokemonData.size)

        listener()

    }

    private fun setupRecycler() {
        binding.apply {
            rvPokemon.adapter = pokemonAdapter
            gridLayoutManager = GridLayoutManager(this@MainActivity,2)
            gridLayoutManager.spanSizeLookup = object : SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    return  if (pokemonData[position] == null) {
                        2
                    } else 1
                }
            }
            rvPokemon.layoutManager = gridLayoutManager
            pokemonAdapter.submitList(pokemonData)
        }
    }

    private fun listener() {
        binding.apply {
            rvPokemon.addOnScrollListener(object : RecyclerView.OnScrollListener(){
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    if(pokemonData.size < totalCount){
                        visibleItemCount = gridLayoutManager.childCount
                        totalItemCount = gridLayoutManager.itemCount
                        pastVisibleItems = gridLayoutManager.findLastVisibleItemPosition()
                        if(loading){
                            if((visibleItemCount + pastVisibleItems) >= totalItemCount){
                                loading = false
                                getPokemonData(pokemonData.size)
                            }
                        }
                    }
                }
            })
        }
    }

    private fun getPokemonData(offset: Int) {
        showLoader()
        mainViewModel.getPokemonData(20,offset).observe(this , { response ->
            hideLoader()
            when(response){
                is ResponseResource.Success -> {
                    val data = response.data!!
                    totalCount = data.count
                    val oldSize = pokemonData.size
                    val newCount = data.results.size
                    pokemonData.addAll(data.results)
                    pokemonAdapter.notifyItemRangeInserted(oldSize,newCount)
                    loading = true
                }
                is ResponseResource.Error -> {

                }
            }
        })
    }

    private fun showLoader(){
        val oldSize = pokemonData.size
        pokemonData.add(null)
        pokemonAdapter.notifyItemInserted(oldSize)
    }

    private fun hideLoader(){
        val oldSize = pokemonData.size - 1
        pokemonData.removeAt(oldSize)
        pokemonAdapter.notifyItemRemoved(oldSize)
    }

    override fun onItemClickListener(index: Int) {
        val message = "${pokemonData[index]!!.name} deleted Successfully"
        pokemonData.removeAt(index)
        pokemonAdapter.notifyItemRemoved(index)
        showToast(message)
    }

}