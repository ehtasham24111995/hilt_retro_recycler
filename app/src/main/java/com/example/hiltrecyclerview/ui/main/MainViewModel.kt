package com.example.hiltrecyclerview.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.hiltrecyclerview.helper.ResponseResource
import com.example.hiltrecyclerview.data.model.Pokemon
import com.example.hiltrecyclerview.data.repository.PokemonRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val pokemonRepository: PokemonRepository
) : ViewModel() {

    fun getPokemonData(limit: Int,offset: Int): LiveData<ResponseResource<Pokemon>> {
        val data = MutableLiveData<ResponseResource<Pokemon>>()
        viewModelScope.launch{
            data.value = pokemonRepository.getPokemonData(limit,offset)
        }
        return data
    }

}