package com.example.hiltrecyclerview.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.hiltrecyclerview.data.model.Pokemon
import com.example.hiltrecyclerview.databinding.AdapterLoaderBinding
import com.example.hiltrecyclerview.databinding.AdapterPokemonBinding
import com.example.hiltrecyclerview.di.PokemonInterface
import javax.inject.Inject

class PokemonAdapter @Inject constructor(
    private val pokemonInterface: PokemonInterface
) : ListAdapter<Pokemon.Result,RecyclerView.ViewHolder>(DiffCallback()) {

    private val viewType : Int = 0
    private val loaderType : Int = 1

    inner class ViewHolder(private val binding: AdapterPokemonBinding) : RecyclerView.ViewHolder(binding.root){
        fun bind(data: Pokemon.Result){
            binding.apply{
                tvName.text = data.name
                Glide.with(root)
                    .load(String.format("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/%d.png",adapterPosition+1))
                    .into(ivAvatar)
                root.setOnClickListener {
                    pokemonInterface.onItemClickListener(adapterPosition)
                }
            }
        }
    }

    inner class LoaderHolder(binding: AdapterLoaderBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewHolderType: Int): RecyclerView.ViewHolder {
        return when(viewHolderType){
            viewType -> {
                ViewHolder(AdapterPokemonBinding.inflate(LayoutInflater.from(parent.context),parent,false))
            }
            else -> {
                LoaderHolder(AdapterLoaderBinding.inflate(LayoutInflater.from(parent.context),parent,false))
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if(holder is ViewHolder) {
            val item = getItem(position)
            holder.bind(item)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when(getItem(position)){
            null -> { loaderType }
            else -> { viewType }
        }
    }

    class DiffCallback : DiffUtil.ItemCallback<Pokemon.Result>(){
        override fun areItemsTheSame(oldItem: Pokemon.Result, newItem: Pokemon.Result): Boolean {
            return oldItem.url == newItem.url
        }

        override fun areContentsTheSame(oldItem: Pokemon.Result, newItem: Pokemon.Result): Boolean {
            return oldItem == newItem
        }
    }
}