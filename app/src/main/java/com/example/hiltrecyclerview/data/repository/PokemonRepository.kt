package com.example.hiltrecyclerview.data.repository

import com.example.hiltrecyclerview.helper.ResponseResource
import com.example.hiltrecyclerview.data.model.Pokemon
import com.example.hiltrecyclerview.network.ApiInterface
import java.lang.Exception
import javax.inject.Inject

class PokemonRepository @Inject constructor(
    private val apiInterface: ApiInterface
) {

    suspend fun getPokemonData(limit: Int,offset: Int) : ResponseResource<Pokemon>{
        return try {
            val response = apiInterface.getPokemonData(limit,offset)
            val result = response.body()
            if(response.isSuccessful && result != null){
                ResponseResource.Success(false,result)
            }else{
                ResponseResource.Error(true,response.message())
            }
        }catch (e: Exception){
            ResponseResource.Error(true,e.localizedMessage)
        }
    }

}