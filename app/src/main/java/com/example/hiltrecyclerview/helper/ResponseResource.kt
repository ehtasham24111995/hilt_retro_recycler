package com.example.hiltrecyclerview.helper

sealed class ResponseResource<T> (val error: Boolean, val data : T? , val message: String?) {
    class Success<T> (error: Boolean,data: T)
        : ResponseResource<T>(error,data,null)
    class Error<T> (error: Boolean,message: String?)
        : ResponseResource<T> (error,null,message)
}