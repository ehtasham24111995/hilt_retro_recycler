package com.example.hiltrecyclerview.di

import android.app.Activity
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.scopes.ActivityScoped

@Module
@InstallIn(ActivityComponent::class)
object PokemonInterfaceModule {

    @Provides
    @ActivityScoped
    fun getPokemonInterface(
        activity: Activity
    ) = activity as PokemonInterface

}

interface PokemonInterface {
    fun onItemClickListener(index: Int)
}
