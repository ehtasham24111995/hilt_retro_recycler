package com.example.hiltrecyclerview.network

import com.example.hiltrecyclerview.data.model.Pokemon
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {

    @GET("v2/pokemon")
    suspend fun getPokemonData(
        @Query("limit") limit: Int,
        @Query("offset") offset: Int
    ) : Response<Pokemon>

}